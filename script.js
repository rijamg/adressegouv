
  document.addEventListener('DOMContentLoaded', function() {
    
  // Initialisation du Plan
  let map;
  initMap();

  //Séléction du bouton rechercher pour récupérer la valeur de l'input
  document
  .querySelector("#search")
  .addEventListener('click', searchCoord)


  // Fonction Recherche des coordonnées en fonction de la valeur recherchée
  function searchCoord() {
  // acquisition de la valeur contenue dans l'input
    let address = document.querySelector("#location").value 

    // Regex pour remplacer tous les caractères spéciaux, y compris l'espace
    address = address.replace(/[^a-zA-Z0-9]/g, "+");

    // test conditionnel pour le cas ou on appui sur rechercher sans rentrer de valeur ou avec une taille de requète trop petite
    if (address && address.length > 10) {
      // Ouverture de l'objet request
      const request = new window.XMLHttpRequest()
      // envoi de la requète à l'api
      request.open('GET', "https://api-adresse.data.gouv.fr/search/?q="+address+"&limit=5")
      request.send()
      // traitement de la réponse 
      request.onreadystatechange = function(){
        
        if (this.readyState === 4 && this.status === 200){
          
          let response = JSON.parse(this.responseText)

          let addressList = []

          // Enregistrement des valeurs reçues dans un tableau d'objets pour traitement ulterieur
          for (let i=0; i<response.features.length; i++){
            let coordonnee = new Object();
            coordonnee.label = response.features[i].properties.label
            coordonnee.longitude = response.features[i].geometry.coordinates[0]
            coordonnee.latitude = response.features[i].geometry.coordinates[1]
            addressList[i]= coordonnee;
          }


          // Création HTML de la liste à afficher
          let ulToPopulate = document.querySelector(".results")

          // Si une liste existe déja, supprimer d'abord l'existant
          if (ulToPopulate.hasChildNodes()) {
            while (ulToPopulate.firstChild) {
              ulToPopulate.removeChild(ulToPopulate.firstChild);
            }
          }

          // Empiler la liste des résultats
          for (let i=0; i<addressList.length; i++){
            let li = document.createElement('li')
            li.setAttribute("class","list")
            li.setAttribute("accessKey",i)
            ulToPopulate.appendChild(li)
            li.innerText = addressList[i].label
          }

          // Si pas de résultat,  affiche une réponse à l'utilisateur qu'il n'y a pas de résultat 
          if (addressList.length === 0){
            let ulToPopulate = document.querySelector(".results")
            let li = document.createElement('li')
            ulToPopulate.appendChild(li)
            li.innerText = "Pas de Résultalt"
          }
          
          
          // Selectionner la liste des résultats
          let list = document.querySelectorAll(".list")
          
          // Ajouter un evenement "click" sur la liste pour affichage sur plan
          list.forEach(el => el.addEventListener('click', event => {
            let selectedAdress = el.accessKey;
            let long = addressList[selectedAdress].longitude
            let lat = addressList[selectedAdress].latitude
    
            if (long !== undefined && lat !== undefined) {
              placeMarker(lat, long)
            }
          }));
        }
      }
    }
  }

    // ---------------------------
    // Ne pas toucher le code en-dessous
    // ---------------------------

    // Créer la carte
    function initMap() {
      const location = [45.56458329528508, 3.3287064863602662];
      map = L.map('map').setView(location, 4);
      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 18}).addTo(map);
    }

    // Ajouter un marqueur à la carte et centrer la carte sur ce point
    function placeMarker(latitude, longitude) {
      const location = [latitude, longitude];
      map.panTo(location);
      L.marker(location).addTo(map);
    }
  })

