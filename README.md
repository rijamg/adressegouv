# Adresses à Afficher sur plan
- API à appeler qui retourne des adresses correspondant à la recherche (Exemple pour "70 rue stanislas nancy" -> https://api-adresse.data.gouv.fr/search/?q=70+rue+stanislas+nancy )
- Carte géographique
- Fonction pour ajouter un marqueur à la carte

Voici les étapes à réaliser :
1.  Créer un formulaire avec un champ texte et un bouton rechercher
2.  Quand le formulaire est soumis, appeler l'API data.gouv.fr avec la valeur du champ texte du formulaire (attention, les espaces doivent être convertis en +, ex: "70 rue stanislas nancy" -> "70+rue+stanislas+nancy")
3.  Récupérer le résultat de cette API et afficher dans l'interface les adresses
4.  Pouvoir cliquer sur une adresse et positionner avec sa latitude longitude associée, le marqueur sur la carte (la fonction )

Lien Pages : https://rijamg.gitlab.io/adressegouv/